//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package main

import (
	"cluster-manager/cmd/kumorimgr/cmd"
	"cluster-manager/pkg/logger"
	"fmt"
)

func init() {
	err := logger.Init("warn", []string{"stdout"}, "console", true)
	if err != nil {
		fmt.Println("Error initializing log", err.Error())
	}
}

func main() {
	cmd.Execute()
}
