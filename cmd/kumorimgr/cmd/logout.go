//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package cmd

import (
	"cluster-manager/pkg/admission"
	"cluster-manager/pkg/logger"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// logoutCmd represents the logout command
var logoutCmd = &cobra.Command{
	Use:   "logout",
	Short: "Logout from Kumori Platform",
	Long: `
Logout from Kumori Platform.

This command invalidates and removes tokens from config file.`,
	Run: func(cmd *cobra.Command, args []string) { runLogout(cmd, args) },
}

func init() {
	rootCmd.AddCommand(logoutCmd)
}

func runLogout(cmd *cobra.Command, args []string) {
	meth := "logout.runLogout()"
	logger.Info("Logout", "meth", meth)
	err := admission.Logout()
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("Logout OK")
	return
}
