//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package cmd

import (
	"cluster-manager/pkg/admission"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login <user>",
	Short: "Login to Kumori Platform",
	Long: `
Login to Kumori Platform.

This command interactively asks for a password and authenticates against Admission service to obtain access and refresh tokens, and their expiry dates. This is saved in config file and is used for later interactions with the Platform.

Tokens are dynamically renewed during kumorictl usage. Once refresh token expires, user will need to login again.`,
	Args: cobra.ExactArgs(1),
	Run:  func(cmd *cobra.Command, args []string) { runLogin(cmd, args) },
}

func init() {
	rootCmd.AddCommand(loginCmd)
}

func runLogin(cmd *cobra.Command, args []string) {
	meth := "login.runLogin()"
	logger.Info("Login", "meth", meth)
	password, err := helper.GetPasswd("Enter Password: ")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	user := args[0]
	err = admission.Login(user, password)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	fmt.Println("Login OK")
	return
}
