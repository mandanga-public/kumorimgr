//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package cmd

import (
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"os"

	"github.com/spf13/cobra"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: `Update the cluster`,
	Long: `
Using the new configuration stored in the './cluster' directory of the workspace, updates the cluster.

Old configuration (that is: current configuration) must be provided too, using the --current-configuration parameter (for example, in the './cluster-old directory).

Currently, supermaster and ingress nodes can't be updated.
Only changes to the list of nodes are processed; any other changes will be ignored.

It is required to have installed the CUE utility (https://cuelang.org/docs/install)`,
	Run: func(cmd *cobra.Command, args []string) { runUpdate(cmd, args) },
}

func init() {
	rootCmd.AddCommand(updateCmd)
	updateCmd.Flags().StringP(
		"current-configuration", "c", "./cluster-old",
		"Directory with the old configuration",
	)
}

func runUpdate(cmd *cobra.Command, args []string) {
	meth := "update.runUpdate()"
	currentCfgDir, err := cmd.Flags().GetString("current-configuration")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	newCfgDir := "./cluster"
	logger.Info(
		"Updating cluster", "currentCfgDir", currentCfgDir, "newCfgDir", newCfgDir,
		"meth", meth,
	)

	// Old configuration
	currentCluster, err := getClusterConfiguration(currentCfgDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// New configuration
	newCluster, err := getClusterConfiguration(newCfgDir)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Calculate node differences between old and new configuration
	logger.Debug("Creating update configuration", "meth", meth)
	updateParams, err := calculateUpdateParams(currentCluster, newCluster)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	newCluster.UpdateParams = updateParams

	// TODO : decoupling from the script structure
	// Generates /scripts/scripts/installer/variables.sh, used by the scripts
	logger.Debug(
		"Generating variables for scripts",
		"content", newCluster, "meth", meth,
	)
	resultFile := "./scripts/scripts/updater/variables.sh"
	err = prepareVarsFile(resultFile, newCluster)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Launch scripts
	err = launchScript("./scripts/scripts/updater/updater.sh", "")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}

func calculateUpdateParams(
	oldCluster *types.ClusterAndDistribution, newCluster *types.ClusterAndDistribution,
) (
	updateParams types.UpdateParams, err error,
) {
	updateParams = types.UpdateParams{
		AddMastersIPs:    []string{},
		AddWorkersIPs:    []string{},
		RemoveMastersIPs: []string{},
		RemoveWorkersIPs: []string{},
	}

	// Nodes to be removed
	removeMachines, err := clusterSubstraction(oldCluster.Cluster, newCluster.Cluster)
	if err != nil {
		return
	}
	for _, m := range removeMachines {
		if m.IsRole(types.MasterRole) {
			updateParams.RemoveMastersIPs = append(updateParams.RemoveMastersIPs, m.IP)
		} else if m.IsRole(types.WorkerRole) {
			updateParams.RemoveWorkersIPs = append(updateParams.RemoveWorkersIPs, m.IP)
		}
	}

	// Nodes to be added
	addMachines, err := clusterSubstraction(newCluster.Cluster, oldCluster.Cluster)
	if err != nil {
		return
	}
	for _, m := range addMachines {
		if m.IsRole(types.MasterRole) {
			updateParams.AddMastersIPs = append(updateParams.AddMastersIPs, m.IP)
		} else if m.IsRole(types.WorkerRole) {
			updateParams.AddWorkersIPs = append(updateParams.AddWorkersIPs, m.IP)
		}
	}

	return
}

// clusterSubstraction returns machines (masters, workers) of clusterA that are
// not in clusterB
func clusterSubstraction(
	clusterA types.Cluster, clusterB types.Cluster,
) (
	machines []types.Machine,
	err error,
) {
	machines = []types.Machine{}
	for _, machine := range clusterA.Machines {
		if !clusterContains(clusterB, machine) {
			machines = append(machines, machine)
		}
	}
	return
}
