//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package cmd

import (
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Set application configuration",
	Long: `
This command sets configuration parameters passed as flags on config file.

Only the specified configuration parameters with be replaced, others will be kept.

When using ` + "`--show`" + ` flag, configuration will be printed instead of being modified on file.`,
	Run: func(cmd *cobra.Command, args []string) { runConfig(cmd, args) },
}

func init() {
	rootCmd.AddCommand(configCmd)
	configCmd.Flags().StringP(
		"admission", "a", viper.Global.GetString("admission"),
		"Admission bare URL (e.g. admission.test.kumori.cloud)",
	)
	viper.Global.BindPFlag("admission", configCmd.Flags().Lookup("admission"))
	configCmd.Flags().String(
		"admission-protocol", viper.Global.GetString("admission-protocol"),
		"Admission protocol (http/https)",
	)
	viper.Global.BindPFlag("admission-protocol", configCmd.Flags().Lookup("admission-protocol"))
	configCmd.Flags().StringP(
		"log-level", "l", viper.Global.GetString("log-level"),
		"kumorictl log level",
	)
	viper.Global.BindPFlag("log-level", configCmd.Flags().Lookup("log-level"))
	configCmd.Flags().Bool("show", false, "prints configuration instead of saving it (dry-run)")
}

func runConfig(cmd *cobra.Command, args []string) {
	meth := "config.runConfig()"
	logger.Info("Config", "meth", meth)

	show, err := cmd.Flags().GetBool("show")
	if err != nil {
		logger.Error("Error accesing 'show' flag: "+err.Error(), "meth", meth)
		os.Exit(1)
	}

	if show {
		for key, value := range viper.Global.AllSettings() {
			if strings.HasSuffix(key, "-token") {
				fmt.Println(key+":", "<hidden>")
			} else {
				fmt.Println(key+":", value)
			}
		}
	} else {
		err = viper.Global.WriteConfig()
		if err != nil {
			logger.Error("Workspace initialization is mandatory to use this command. "+err.Error(), "meth", meth)
			os.Exit(1)
		}
	}

	return
}
