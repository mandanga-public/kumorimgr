//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package cmd

const KumoriMgrVersion = "0.3.8"
const AppConfigVersion = "0.1.0"
const AppConfigFilePerm = 0664
const CueFilePerm = 0664

// DefaultAppConfig is used to initialize the .kumori/kumorimgr.json file
const DefaultAppConfig = `{
  "config-version": "0.1.0",
  "log-level": "fatal",
  "admission": "",
  "admission-protocol": "https",
  "scripts": {
    "url": "https://gitlab.com/kumori-systems/community/libraries/kumorimgr-scripts#v0.3.7"
  },
  "cluster-configuration": {
    "community": {
      "url": "https://gitlab.com/kumori-systems/community/distribution.git#v0.1.3"
    }
  }
}`
