//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package cmd

import (
	"cluster-manager/pkg/cue2json"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/tmpl"
	"cluster-manager/pkg/types"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"text/template"
)

func getClusterConfiguration(
	cfgDir string,
) (
	config *types.ClusterAndDistribution, err error,
) {
	meth := "common.getClusterConfiguration()"
	logger.Info("Generating cluster configuration", "cfgDir", cfgDir, "meth", meth)

	cfgDir, err = filepath.Abs(cfgDir)
	if err != nil {
		return
	}

	// Compile cue
	logger.Info("Compiling CUE files", "meth", meth)
	err = cue2json.ExportAll(cfgDir, cfgDir+"/all.json")
	if err != nil {
		return
	}

	// Load json into struct
	logger.Debug("Loading json files into structs", "meth", meth)
	config, err = types.NewClusterAndDistributionFromFile(cfgDir + "/all.json")
	if err != nil {
		return
	}

	return
}

// clusterContains returns true if cluster contains provided machina
// BE CAREFUL: just name is checked!
func clusterContains(cluster types.Cluster, machine types.Machine) bool {
	for _, m := range cluster.Machines {
		if reflect.DeepEqual(m, machine) {
			return true
		}
	}
	return false
}

// TODO : decoupling from the script structure
// Launch scripts
func launchScript(fullPathScriptFile string, param string) (err error) {
	meth := "common.launchScript()"
	logger.Info("Launching scripts", "meth", meth, "script", fullPathScriptFile)
	fmt.Println("Launching scripts")
	env := os.Environ()
	path, err := filepath.Abs(fullPathScriptFile)
	if err != nil {
		return
	}
	var cmd *exec.Cmd
	if param != "" {
		//err = syscall.Exec(path, []string{param}, env) --> not working for shell scripts + parameters
		cmd = exec.Command(path, param)
	} else {
		//err = syscall.Exec(path, []string{}, env)
		cmd = exec.Command(path)
	}
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = env
	err = cmd.Run()

	if err != nil {
		return
	}
	return
}

// TODO : decoupling from the script structure
// Generates /scripts/scripts/XXX/variables.sh, taking into account all nodes
func prepareVarsFile(varsFile string, config *types.ClusterAndDistribution) (err error) {
	t := template.Must(template.New("scripts").Parse(tmpl.ScriptsVarsTemplate))
	outputFile, err := os.Create(varsFile)
	if err != nil {
		return
	}
	err = t.Execute(outputFile, config)
	if err != nil {
		return
	}
	outputFile.Close()
	return
}
