//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package cmd

import (
	"cluster-manager/pkg/git"
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/viper"
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	cuedm "gitlab.com/kumori-systems/community/libraries/cue-dependency-manager"
)

// Init command:
// - Removes current configuration
// - Creates a base config from cluster (a cue module)
// - Resolves the cue dependencies of that config, available in git
// - Downloads scripts used for create the cluster, downloaded from git

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: `Initialize a workspace for kumorimgr`,
	Long: `
For kumorimgr to work, a workspace must be initialized in a directory.

This command creates a base configuration in './cluster' directory, with cluster.cue, distribution.cue and config.cue files (and its 'cue' dependencies).
This command creates the scripts, in './scripts' directory, used by kumorimgr to create a new cluster.

If './cluster' or './scripts' already exists, a backup is created.

Moreover, it creates the config file required to use kumorimgr in .kumori directory. In order to create config file in your HOME directory, set ` + "`--global`" + ` flag. You can also use ` + "`--config`" + ` flag to specify a custom location.

The config file is created using global config or, if missing, defaults. To customize configuration parameters, then use ` + "`kumorictl config`" + `.

Please note this command will purposely fail if the current directory has already been initialized as a workspace.`,
	Run: func(cmd *cobra.Command, args []string) { runInit(cmd, args) },
}

func init() {
	rootCmd.AddCommand(initCmd)
	initCmd.Flags().BoolP("global", "g", false, "write config file in HOME directory")
	initCmd.Flags().StringP(
		"distribution", "d", "community",
		"Distribution to be used in the cluster",
	)
}

func runInit(cmd *cobra.Command, args []string) {
	meth := "init.runInit()"
	distribution, err := cmd.Flags().GetString("distribution")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	initCreateConfigFile(cmd)
	initBackupWorkspace()
	initCreateWorkspace(distribution)
}

func initCreateConfigFile(cmd *cobra.Command) {
	meth := "init.initCreateConfigFile()"
	if cfgFile == "" {
		logger.Debug("Creating config file", "meth", meth)
		global, err := cmd.Flags().GetBool("global")
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
		if global {
			viper.Global.SetConfigFile(home + "/.kumori/kumorimgr.json")
		} else {
			viper.Global.SetConfigFile("./.kumori/kumorimgr.json")
		}
		writeDefaultConfigFile()
	}
}

func initBackupWorkspace() {
	meth := "init.initBackupWorkspace()"

	// Creates a backup of current directories
	logger.Info("Creating workspace backup", "meth", meth)
	nowStr := time.Now().Format("20060102150405")
	err := helper.RenameIfExists("./cluster", "./bak/cluster_"+nowStr)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	err = helper.RenameIfExists("./distribution", "./bak/distribution_"+nowStr)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	err = helper.RenameIfExists("./scripts", "./bak/scripts_"+nowStr)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
}

func initCreateWorkspace(distribution string) {
	meth := "init.initCreateWorkspace()"

	// Creates a the cluster configuration directory, using git
	// Creates the scripts directory, using git
	logger.Info("Cloning configuration", "meth", meth)
	clusterDistribution := "cluster-configuration::" + distribution
	if !viper.Global.IsSet(clusterDistribution) {
		err := fmt.Errorf("Distribution %s not found", distribution)
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	var credConfig *git.Credentials = nil
	credConfig, err := git.NewCredentialsFromConfig(clusterDistribution)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	clusterURL := viper.Global.GetString(clusterDistribution + "::url")
	err = git.Clone("./cluster", clusterURL, credConfig)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	// Resolve dependencies of the cluster (cue) directory, using git
	logger.Debug("Resolving cue dependencies (kumori-model)", "meth", meth)
	src := "./cluster"
	ctx, err := cuedm.Resolve(src)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	for _, module := range ctx.CUEModules {
		logger.Debug(
			"Cluster-Dependency",
			"Module", module.Name,
			"Resolved", module.Resolved,
			"meth", meth,
		)
	}

	// Creates the scripts directory, using git
	logger.Info("Cloning scripts", "meth", meth)
	credScripts, err := git.NewCredentialsFromConfig("scripts")
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}
	scriptsURL := viper.Global.GetString("scripts::url")
	err = git.Clone("./scripts", scriptsURL, credScripts)
	if err != nil {
		logger.Error(err.Error(), "meth", meth)
		os.Exit(1)
	}

	fmt.Println("Workspace initialized")
}

func writeDefaultConfigFile() {
	meth := "init.writeDefaultConfigFile()"
	filePath := viper.Global.ConfigFileUsed()
	logger.Info("Writing config file: "+filePath, "meth", meth)
	if !helper.FileExists(filePath) {
		logger.Info("Creating config file", "file", filePath, "meth", meth)
		err := helper.CreateFile(filePath, AppConfigFilePerm, []byte(DefaultAppConfig))
		if err != nil {
			logger.Error(err.Error(), "meth", meth)
			os.Exit(1)
		}
	}
}
