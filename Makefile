
run-help: fmt vet
	go run ./cmd/kumorimgr/main.go --help

run-version: fmt vet
	go run ./cmd/kumorimgr/main.go --version

run-login: fmt vet
	go run ./cmd/kumorimgr/main.go login

run-logout: fmt vet
	go run ./cmd/kumorimgr/main.go logout

build: fmt vet
	go build -o bin/kumorimgr ./cmd/kumorimgr/main.go

build-macos: fmt vet
	GOOS=darwin GOARCH=amd64 go build -o bin/kumorimgr-darwin-amd64 ./cmd/kumorimgr/main.go

fmt:
	go fmt ./...

vet:
	go vet ./...
