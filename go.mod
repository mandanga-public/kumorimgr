module cluster-manager

go 1.12

// require (
//   go.uber.org/zap v1.13.0
//   github.com/mitchellh/go-homedir v1.1.0
//   github.com/spf13/cobra v1.0.0
//   github.com/spf13/viper v1.7.0
//   gopkg.in/src-d/go-git.v4 v4.13.1
//   gitlab.com/kumori-systems/community/libraries/cue-dependency-manager v0.1.3
//   golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
//   github.com/Jeffail/gabs v1.4.0
//   github.com/ghodss/yaml v1.0.0
// )

require (
	github.com/Jeffail/gabs v1.4.0
	github.com/fatih/color v1.9.0
	github.com/ghodss/yaml v1.0.0
	github.com/ldez/go-git-cmd-wrapper v1.3.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/olekukonko/tablewriter v0.0.4
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	gitlab.com/kumori-systems/community/libraries/cue-dependency-manager v0.1.3
	go.uber.org/zap v1.13.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	gopkg.in/src-d/go-git.v4 v4.13.1
	k8s.io/apimachinery v0.18.3 // indirect
)
