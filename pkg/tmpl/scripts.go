//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package tmpl

// TODO: TO IMPROVE!!! How manage better? Gitlab repository with templates?

const ScriptsVarsTemplate = `
#!/bin/bash

################################################################################
## CLUSTER MACHINES IP ADDRESSES AND CONNECTIVITY                             ##
################################################################################

# MASTERS
MASTERS_IPS=(
  {{range $mkey, $machine := .Cluster.Machines}}{{range $lkey, $label := $machine.Labels}}{{if eq $label.Noderole "master"}}"{{$machine.IP}}"
  {{ end }}{{ end }}{{ end }})

# WORKERS
WORKERS_IPS=(
  {{range $mkey, $machine := .Cluster.Machines}}{{range $lkey, $label := $machine.Labels}}{{if eq $label.Noderole "worker"}}"{{$machine.IP}}"
  {{ end }}{{ end }}{{ end }})

# Nodes that will be Ingress entrypoints
INGRESS_NODES_IPS=(
  {{range $mkey, $machine := .Cluster.Ingress.Nodes}}"{{$machine.IP}}"
  {{ end }})

# CLUSTER UPDATE DEFINITIONS
ADD_MASTERS_IPS=(
  {{range .UpdateParams.AddMastersIPs}}"{{.}}"
  {{ end }})
ADD_WORKERS_IPS=(
  {{range .UpdateParams.AddWorkersIPs}}"{{.}}"
  {{ end }})
REMOVE_MASTERS_IPS=(
  {{range .UpdateParams.RemoveMastersIPs}}"{{.}}"
  {{ end }})
REMOVE_WORKERS_IPS=(
  {{range .UpdateParams.RemoveWorkersIPs}}"{{.}}"
  {{ end }})

# SSH CONNECTION DETAILS
SSH_USER="{{.Cluster.SSH.User}}"
SSH_KEY="{{.Cluster.SSH.PrivateKeyPath}}"

# BASIC CONFIGURATION
DEFAULT_LOCALE="es_ES.UTF-8"
TIMEZONE="CET"

################################################################################
## INSTALLATION CUSTOMIZATION PROPERTIES                                      ##
################################################################################

RELEASE_NAME="{{.Cluster.Name}}"
REFERENCE_DOMAIN="{{.Cluster.DNS.ReferenceDomain}}"

#######################
##     APISERVER     ##
#######################

# APISERVER - GENERAL SETTINGS
#
# ApiServer Endpoint configuration (ApiServer domain, LB IP or master IP)
APISERVER_DOMAIN="apiserver-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
APISERVER_PORT="8443"
APISERVER_URL="${APISERVER_DOMAIN}:${APISERVER_PORT}"
# Domains or IPs to be added as accepted in ApiServer TLS certificate
# If several, set as comma separated string list (no spaces)
APISERVER_CERT_EXTRA_SANS=""

# APISERVER - MANAGE APISERVER DNS DOMAIN
#
# If enabled, the folowing configuration settings are mandatory:
# - APISERVER_DNS_IP / APISERVER_DNS_CNAME : the IP or CNAME to be registered in DNS for ApiServer domain
# - APISERVER_DOMAIN_TTL
APISERVER_REGISTER_DOMAIN="{{.Cluster.DNS.Management.Enabled}}"
APISERVER_DNS_IP={{.GetAPIServerExternalIP}}
APISERVER_DNS_CNAME=""
APISERVER_DNS_TTL="300"

# APISERVER - INTERNAL BALANCING
#
# Install an internal load-balancer in every Master node to balance ApiServer
# requests among all endpoints.
# Internal load-balancers are automatically configured to listen on APISERVER_PORT
# and their target list includes all Master nodes at ApiServer default port (6443).
APISERVER_INTERNAL_BALANCING={{.GetAPIServerInternalBalancing}}

# APISERVER - HANDLE FLOATING IP
#
# User provides a Virtual IP for ApiServer, and the platform is in charge of ensuring
# that IP is always assigned to a healthy Master node.
# If enabled, the folowing configuration settings are mandatory:
# - APISERVER_VIRTUAL_IP           (user provided)
APISERVER_HANDLE_FLOATING_IP={{.GetAPIServerHandleFloatingIP}}
APISERVER_VIRTUAL_IP={{.GetAPIServerInternalIP}}

#####################
##     INGRESS     ##
#####################
# INGRESS - GENERAL SETTINGS
#
# Ingress domain
INGRESS_DOMAIN="ingress-${RELEASE_NAME}.${REFERENCE_DOMAIN}"
# Namespace where the Ingress controller will be deployed
INGRESS_NAMESPACE="kumori"

# INGRESS - INTERNAL BALANCING
#
# Install an internal load-balancer in every Ingress node to balance Ingress
# requests among all endpoints.
# Internal load-balancers are automatically configured to listen on ports 80 and 443
# and their target list includes all Ingress nodes at these ports.
INGRESS_INTERNAL_BALANCING={{.GetIngressInternalBalancing}}

# INGRESS - HANDLE FLOATING IP
#
# User provides a Virtual IP for Ingress, and the platform is in charge of ensuring
# that IP is always assigned to a healthy Ingress node.
# If enabled, the folowing configuration settings are mandatory:
# - INGRESS_VIRTUAL_IP           (user provided)
INGRESS_HANDLE_FLOATING_IP={{.GetIngressHandleFloatingIP}}
INGRESS_VIRTUAL_IP={{.GetIngressInternalIP}}

# INGRESS - MANAGE INGRESS DNS DOMAIN
#
# If enabled, the folowing configuration settings are mandatory:
# - INGRESS_DOMAIN
# - INGRESS_DNS_IP / INGRESS_DNS_CNAME: the IP or CNAME to be registered in DNS for ingress domain
# - INGRESS_DOMAIN_TTL
INGRESS_REGISTER_DOMAIN="{{.Cluster.DNS.Management.Enabled}}"
INGRESS_DNS_IP={{.GetIngressExternalIP}}
INGRESS_DNS_CNAME=""
INGRESS_DNS_TTL="300"

############################
##     DNS MANAGEMENT     ##
############################
# If true, the platform is in charge of managing *all* cluster related DNS domains.
# If false, the operator is in charge of managing *all* cluster related DNS domains.
MANAGED_DNS="{{.Cluster.DNS.Management.Enabled}}"
MANAGED_DNS_PROVIDER={{.GetDNSProviderName}}
MANAGED_DNS_ROUTE53_CLI_IMAGE="amazon/aws-cli:2.0.8"
MANAGED_DNS_ROUTE53_AWS_CONFIG_DIR={{.GetDNSRoute53ConfigDir}}

########################
##     NETWORKING     ##
########################
# IMPORTANT WARNINGS:
# - these CIDRs should never overlap
# - these CIDRs should never overlap with other clusters on the same network
# - Docker CIDR must be based on a valid IP, since that IP will be set for the
#   docker0 interface. This means the broadcast address (x.x.x.0) IS NOT valid.
# - CIDR syntax (reminder):
#     - 172.20.0.1/24  -->  172.20.0.x
#     - 172.20.0.1/16  -->  172.20.x.x
#     - 172.20.0.1/8   -->  172.x.x.x
# - Valid private CIDR (reminder)
#     - 10.0.0.0    - 10.255.255.255
#     - 172.16.0.0  - 172.31.255.255
#     - 192.168.0.0 - 192.168.255.255
DOCKER_BRIDGE_CIDR="{{.Cluster.DockerBridgeNet}}"
PODS_NETWORK_CIDR="{{.Cluster.PodNet}}"
SERVICE_CIDR="{{.Cluster.ClusterIPNet}}"

# Optional: DNS resolvers for replacing DHCP configuration.
# This is *very* recommended for clusters deployed on ITI's Openstack.
DNS_RESOLVERS=(
  {{range $mkey, $ip := .Cluster.DNSResolvers}}"{{$ip}}"
  {{ end }})

####################
##     ADDONS     ##
####################

ADDONS_STORAGE_CLASS="rook-ceph-block-ec-ext4"

PROMETHEUS_REPLICAS="1"
PROMETHEUS_PERSISTENCE="false"
PROMETHEUS_REMOTE_READ_URL="not-aplicable"
PROMETHEUS_REMOTE_WRITE_URL="not-aplicable"

INSTALL_ELASTICSEARCH="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "elasticsearch"}}{{$packagedep.Configuration.enabled}}{{ end }}{{ end }}"
INSTALL_KIBANA="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kibana"}}{{$packagedep.Configuration.enabled}}{{ end }}{{ end }}"

INSTALL_FILEBEAT="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "filebeat"}}{{$packagedep.Configuration.enabled}}{{ end }}{{ end }}"
FILEBEAT_ELASTICSEARCH_URL="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "filebeat"}}{{$packagedep.Configuration.elasticsearchUrl}}{{ end }}{{ end }}"
FILEBEAT_ELASTICSEARCH_USERNAME="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "filebeat"}}{{$packagedep.Configuration.elasticsearchUserName}}{{ end }}{{ end }}"
FILEBEAT_ELASTICSEARCH_PASSWORD="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "filebeat"}}{{$packagedep.Configuration.elasticsearchPassword}}{{ end }}{{ end }}"
FILEBEAT_INDEX_PATTERN="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "filebeat"}}{{$packagedep.Configuration.indexPattern}}{{ end }}{{ end }}"

KEYCLOAK_ADMIN_USERNAME="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "keycloak"}}{{$packagedep.Configuration.adminUserName}}{{ end }}{{ end }}"
KEYCLOAK_ADMIN_PASSWORD="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "keycloak"}}{{$packagedep.Configuration.adminPassword}}{{ end }}{{ end }}"

INSTALL_MINIO="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "minio"}}{{$packagedep.Configuration.enabled}}{{ end }}{{ end }}"
MINIO_ACCESS_KEY="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "minio"}}{{$packagedep.Configuration.accessKey}}{{ end }}{{ end }}"
MINIO_SECRET_KEY="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "minio"}}{{$packagedep.Configuration.secretKey}}{{ end }}{{ end }}"
MINIO_ETCD_BUCKET="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "minio"}}{{$packagedep.Configuration.etcdBucket}}{{ end }}{{ end }}"

INSTALL_ETCD_BACKUP="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.enabled}}{{ end }}{{ end }}"
ETCD_BACKUP_ETCD_ENDPOINT="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.etcdEndpoint}}{{ end }}{{ end }}"
ETCD_BACKUP_SCHEDULE="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.schedule}}{{ end }}{{ end }}"
ETCD_BACKUP_DELTA_PERIOD="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.deltaPeriod}}{{ end }}{{ end }}"
ETCD_BACKUP_S3_ENDPOINT="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.s3Endpoint}}{{ end }}{{ end }}"
ETCD_BACKUP_S3_ACCESS_KEY="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.s3AccessKey}}{{ end }}{{ end }}"
ETCD_BACKUP_S3_SECRET_KEY="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.s3SecretKey}}{{ end }}{{ end }}"
ETCD_BACKUP_S3_BUCKET="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.s3Bucket}}{{ end }}{{ end }}"
ETCD_BACKUP_TRUSTED_CA_CONFIGMAP="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}{{$packagedep.Configuration.trustedCAConfigmap}}{{ end }}{{ end }}"

PKI_BACKUP="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "pkibackup"}}{{$packagedep.Configuration.enabled}}{{ end }}{{ end }}"
PKI_BACKUP_S3_ENDPOINT="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "pkibackup"}}{{$packagedep.Configuration.s3Endpoint}}{{ end }}{{ end }}"
PKI_BACKUP_S3_ACCESS_KEY="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "pkibackup"}}{{$packagedep.Configuration.s3AccessKey}}{{ end }}{{ end }}"
PKI_BACKUP_S3_SECRET_KEY="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "pkibackup"}}{{$packagedep.Configuration.s3SecretKey}}{{ end }}{{ end }}"
PKI_BACKUP_S3_BUCKET="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "pkibackup"}}{{$packagedep.Configuration.s3Bucket}}{{ end }}{{ end }}"

# ExternalDNS will only be installed if platform is in charge of DNS management.
# It will be configured according to the MANAGED_DNS_PROVIDER.
INSTALL_EXTERNALDNS="${MANAGED_DNS}"
EXTERNALDNS_OWNER_ID="kumori-cluster-${RELEASE_NAME}"

################################################################################
## PATHS                                                                      ##
################################################################################

CUSTOM_DIR={{.GetCustomScriptsDirectory}}
JOIN_SCRIPTS_DIR="${LOCAL_WORKDIR}/join-scripts" # Local path

################################################################################
## ETCD BACKUP                                                                ##
################################################################################

USE_ETCDBACKUP="{{.InlineParams.UseEtcdBackup}}"
ETCDBACKUP_FILE="{{.InlineParams.EtcdBackupFile}}"

################################################################################
## VERSIONS                                                                   ##
################################################################################
OS_FLAVOUR="{{.Distribution.OSVersion.Flavour}}"
OS_VERSION="{{.Distribution.OSVersion.Version}}"
KERNEL_VERSION="{{.Distribution.OSVersion.Kernel}}"
SYSTEMD_VERSION="{{.Distribution.OSVersion.Systemd}}"
KUBERNETES_VERSION="{{.Distribution.KubeVersion}}"
DOCKER_VERSION="18.06.3~ce~3-0~ubuntu"
HELM_VERSION="v2.16.10"
KEEPALIVED_VERSION="1:1.3.9*"
ENVOY_VERSION="1.15*"

CALICO_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "calico"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
AMBASSADOR_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "ambassador"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KUBE_PROMETHEUS_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kube-prometheus"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
ELASTICSEARCH_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "elasticsearch"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
FILEBEAT_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "filebeat"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KIBANA_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kibana"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
K8S_DASHBOARD_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kubernetes-dashboard"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KEYCLOAK_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "keycloak"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
MINIO_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "minio"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
MINIO_MC_VERSION="{{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "minio"}}{{$packagedep.Configuration.minioMcVersion}}{{ end }}{{ end }}"
ETCD_BACKUP_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "etcdbackup"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
ETCD_RESTORE_IMAGE="k8s.gcr.io/etcd:3.3.15-0"
EXTERNALDNS_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "externaldns"}}{{$packagedep.Package.Version}}{{ end }}{{ end }}

KUMORI_IMAGES_PULL_POLICY="Always"
KUMORI_KUVOLUME_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kumori-volume"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KUMORI_COREDNS_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kumori-coredns"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KUMORI_TOPOLOGY_CONTROLLER_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kumori-topology"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KUMORI_KUCONTROLLER_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kumori-kucontroller"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KUMORI_KUINBOUND_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kumori-kuinbound"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KUMORI_ADMISSION_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kumori-admission"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}
KUMORI_KUALARM_VERSION={{range $i, $packagedep := .Distribution.PackageList}}{{if eq $packagedep.Package.Name "kumori-kualarm"}}"{{$packagedep.Package.Version}}"{{ end }}{{ end }}

################################################################################
## KUMORI CONTROLLERS IMAGE REGISTRY                                          ##
################################################################################

KUMORI_IMAGES_REGISTRY={{.GetControllersHubRegistry}}
KUMORI_IMAGES_REGISTRY_USERNAME={{.GetControllersHubUsername}}
KUMORI_IMAGES_REGISTRY_PASSWORD={{.GetControllersHubPassword}}
`
