//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package alarms

import (
	"cluster-manager/pkg/helper"
	"cluster-manager/pkg/jsonwrapper"
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/types"
	"encoding/json"
	"os"
	"strconv"
)

// U is just an alias to the type allowing manipulate json without go-structs
type U = map[string]interface{}

const (
	ALARMS_SUBDIR        = "alarms"
	ALARMDEFS_SUBDIR     = "alarmdefinitions"
	ALARMIMPLPROM_SUBDIR = "alarmimpls/prometheus"
	DIRPERM              = 0775
	FILEPERM             = 0644
)

func PrepareAlarmFiles(
	alarmDirectory string,
	config *types.ClusterAndDistribution,
) (
	err error,
) {
	meth := "alarms.PrepareAlarmFiles()"
	logger.Debug("Generating alarm related files for scripts", "meth", meth)

	// Ensures directory exists and is empty
	if helper.DirExists(alarmDirectory) {
		_, err = helper.EmptyDir(alarmDirectory)
		if err != nil {
			return
		}
	} else {
		err = os.MkdirAll(alarmDirectory, os.ModeDir|DIRPERM)
		if err != nil {
			return
		}
	}

	// Create subdirectories
	err = os.MkdirAll(alarmDirectory+"/"+ALARMS_SUBDIR, os.ModeDir|DIRPERM)
	if err != nil {
		return
	}
	err = os.MkdirAll(alarmDirectory+"/"+ALARMDEFS_SUBDIR, os.ModeDir|DIRPERM)
	if err != nil {
		return
	}
	err = os.MkdirAll(alarmDirectory+"/"+ALARMIMPLPROM_SUBDIR, os.ModeDir|DIRPERM)
	if err != nil {
		return
	}

	// Get configuration of package kumori-alarm
	alarmCfg, err := config.Distribution.GetPackageConfig("kumori-kualarm")
	if err != nil {
		return
	}

	// Process alarm definitions
	logger.Debug("Generating alarm definitions", "meth", meth)
	alarmDefs, err := jsonwrapper.GetArrayValue(alarmCfg, "alarmdefinitions")
	if err != nil {
		return
	}
	for k, v := range alarmDefs {
		var alarmDefBytes []byte
		alarmDefBytes, err = json.Marshal(v)
		if err != nil {
			return
		}
		filepath := alarmDirectory + "/" + ALARMDEFS_SUBDIR + "/alarmdef_" + strconv.Itoa(k) + ".json"
		err = helper.CreateFile(filepath, FILEPERM, alarmDefBytes)
	}
	logger.Debug("Generated alarm definitions", "meth", meth)

	// Process alarm implementations
	// PROVISIONAL: ASSUMES JUST PROMETHEUS IMPLEMENTATION!
	logger.Debug("Generating alarm implementations", "meth", meth)
	alarmImplProm, err := jsonwrapper.GetArrayValue(alarmCfg, "alarmimpl.prometheus")
	if err != nil {
		return
	}
	for k, v := range alarmImplProm {
		var alarmImplBytes []byte
		alarmImplBytes, err = json.Marshal(v)
		if err != nil {
			return
		}
		filepath := alarmDirectory + "/" + ALARMIMPLPROM_SUBDIR + "/alarmimplprometheus_" + strconv.Itoa(k) + ".json"
		err = helper.CreateFile(filepath, FILEPERM, alarmImplBytes)
	}
	logger.Debug("Generated alarm implementations", "meth", meth)

	// Process alarm instances
	logger.Debug("Generating alarm instance", "meth", meth)
	alarmInstances, err := jsonwrapper.GetArrayValue(alarmCfg, "alarms")
	if err != nil {
		return
	}
	for k, v := range alarmInstances {
		var alarmBytes []byte
		alarmBytes, err = json.Marshal(v)
		if err != nil {
			return
		}
		filepath := alarmDirectory + "/" + ALARMS_SUBDIR + "/alarm_" + strconv.Itoa(k) + ".json"
		err = helper.CreateFile(filepath, FILEPERM, alarmBytes)
	}
	logger.Debug("Generated alarm instance", "meth", meth)

	// Process alarm manager
	logger.Debug("Generating alarm manager configuration", "meth", meth)
	alarmManager, err := jsonwrapper.GetValue(alarmCfg, "alarmmanager")
	if err != nil {
		return
	}
	var alarmManagerCR string
	alarmManagerCR, err = createAlarmManagerCR(alarmManager)
	if err != nil {
		return
	}
	filepath := alarmDirectory + "/alarmmanager.yaml"
	err = helper.CreateFile(filepath, 0644, []byte(alarmManagerCR))
	logger.Debug("Generated alarm manager configuration", "meth", meth)

	return
}
