//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package alarms

import (
	"encoding/json"
	"strings"

	"github.com/ghodss/yaml"
)

func createAlarmManagerCR(alarmmanager interface{}) (alarmManagerCR string, err error) {

	//
	// PROVISIONAL IMPLEMENTATION! GO DEFINITION SHOULD BE USED!!
	//
	// PROVISIONAL IMPLEMENTATION! USING YAML-KUBE-SECRET INSTEAD OF JSON-KUKU CRD
	//

	alarmManagerJSONBytes, err := json.Marshal(alarmmanager)
	if err != nil {
		return
	}
	alarmManagerYAMLBytes, err := yaml.JSONToYAML(alarmManagerJSONBytes)
	if err != nil {
		return
	}
	alarmManagerStr := string(alarmManagerYAMLBytes)

	// Add spaces, to build a valid yaml
	alarmManagerStrSpaced := ""
	for _, line := range strings.Split(strings.TrimSuffix(alarmManagerStr, "\n"), "\n") {
		alarmManagerStrSpaced = alarmManagerStrSpaced + "    " + line + "\n"
	}

	alarmManagerCR = `
apiVersion: "v1"
kind: "Secret"
metadata:
	name: "alertmanager-main"
	namespace: "monitoring"
type: "Opaque"
stringData:
	alertmanager.yaml: |-
` + alarmManagerStrSpaced

	// Tabs are not allowed in YAML
	alarmManagerCR = tabToSpace(alarmManagerCR)

	return
}

func tabToSpace(input string) string {
	var result []string
	for _, c := range input {
		if c == '\t' {
			result = append(result, "  ")
		} else {
			result = append(result, string(c))
		}
	}
	return strings.Join(result, "")
}
