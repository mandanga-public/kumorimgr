//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package git

import (
	"cluster-manager/pkg/helper"
	"fmt"
	"os"
	"path"
	"strings"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
)

// Clone clones a git repository in a given folder
func Clone(dst string, url string, credentials *Credentials) (err error) {

	err = os.MkdirAll(dst, os.ModeDir|0777)
	if err != nil {
		return
	}

	// Checks if we must clone a tag/commit (referenceName) or not
	referenceName := ""
	parts := strings.Split(url, "#")
	lenparts := len(parts)
	if lenparts == 2 {
		url = parts[0]
		referenceName = parts[1]
	} else if lenparts > 2 {
		last := lenparts - 2
		urlparts := parts[0:last]
		url = strings.Join(urlparts, "#")
		referenceName = parts[lenparts-1]
	}

	// Checks if the repository already contains the demanded git repository. If that
	// is the case, pull form the remote repository to update the folder. Otherwise,
	// clean the previous content and clone.
	alreadyCloned := false
	origin := ""
	gitpath := path.Join(dst, ".git")
	if helper.DirExists(gitpath) {
		repo, err := git.PlainOpen(dst)
		if err != nil {
			return fmt.Errorf("Error reading repository '%s': %s", url, err.Error())
		}
		config, err := repo.Config()
		if err != nil {
			return fmt.Errorf("Error getting remotes from repository '%s': %s", url, err.Error())
		}
		for name, remote := range config.Remotes {
			if remote == nil {
				continue
			}
			if len(remote.URLs) <= 0 {
				continue
			}
			for _, u := range remote.URLs {
				if strings.HasSuffix(url, u) {
					origin = name
					alreadyCloned = true
					break
				}
			}
		}

		// Pulls from the remote repo and returns if the destination folder already
		// contains the expected repo
		if alreadyCloned {
			err := pullFromRepo(dst, origin, referenceName)
			if err != nil {
				return err
			}
			return nil
		}
	}

	// Removes content of the destination folder since it isn't empty ant it not
	// contains the expected repository
	_, err = helper.EmptyDir(dst)
	if err != nil {
		return fmt.Errorf("Error removing content from folder '%s': %s", dst, err.Error())
	}

	// Clones the git repository in the target folder and returns
	err = cloneRepo(dst, url, referenceName, credentials)
	if err != nil {
		return
	}
	return nil
}

// pullFromRepo pulls the last changes from a given repo in a given folder.
// referenceName can be a branch or tag short name. If "" then it is not taken into account.
func pullFromRepo(dst string, origin string, referenceName string) error {
	// Opens the repo and reads the worktree
	repo, err := git.PlainOpen(dst)

	if err != nil {
		return fmt.Errorf("Error reading repository '%s': %s", dst, err.Error())
	}
	worktree, err := repo.Worktree()
	if err != nil {
		return fmt.Errorf("Error getting working tree from '%s': %s", dst, err.Error())
	}

	// If the reference name is set, we must check if it is a branch or a repo
	// NOTE: this code is not tested
	iter, err := repo.References()
	if err != nil {
		return err
	}
	branchReference := plumbing.NewRemoteReferenceName(origin, referenceName)
	err = iter.ForEach(func(ref *plumbing.Reference) error {
		if (ref.Name().Short() == referenceName) || (ref.Name().Short() == branchReference.Short()) {
			options := &git.PullOptions{
				RemoteName:    origin,
				ReferenceName: ref.Name(),
			}
			if err = worktree.Pull(options); err != nil {
				return fmt.Errorf("Error pulling repository '%s' from remote '%s': %s", dst, origin, err.Error())
			}
		}
		return nil
	})
	return nil
}

// cloneRepo clones a repo from a given url in a given folder. The destination
// folder is supposed to be created and empty.
// referenceName can be:
// - a commit
// - a tag short name
// - nothing (""), so master is used
// - branchs are not allowed
//
func cloneRepo(dst string, url string, referenceName string, credentials *Credentials) error {

	// Prepare the clone options
	cloneOptions := &git.CloneOptions{
		URL:               url,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
	}

	// Add credentials if any
	if credentials != nil {
		auth := http.BasicAuth{
			Username: credentials.Username,
		}
		switch credentials.CredType {
		case "password":
			auth.Password = *credentials.Password
		case "token":
			auth.Password = *credentials.Token
		default:
			return fmt.Errorf("Invalid credentials. Password or Token not found ")
		}
		cloneOptions.Auth = &auth
	}

	// Clone the repository on master branch
	repo, err := git.PlainClone(dst, false, cloneOptions)
	if err != nil {
		return err
	}

	// What commit must be used? Tag, commit or master?
	// Review all repo references, checking if any match tag or commit
	var finalReference plumbing.Hash
	finalName := ""
	iter, err := repo.References()
	err = iter.ForEach(func(ref *plumbing.Reference) error {
		name, commit, err := getNameAndCommit(ref, repo)
		if err != nil {
			return err
		}
		if referenceName != "" && (referenceName == name || referenceName == commit.String()) {
			finalReference = commit
			finalName = name
		}
		if referenceName == "" && name == "master" {
			finalReference = commit
			finalName = name
		}
		return nil
	})
	if err != nil {
		return err
	}
	if finalName == "" {
		err = fmt.Errorf("No matching reference found in the repository")
		return err
	}
	// Checkout the right commit
	worktree, err := repo.Worktree()
	if err != nil {
		return err
	}
	checkoutOptions := git.CheckoutOptions{}
	checkoutOptions.Hash = finalReference
	if err = worktree.Checkout(&checkoutOptions); err != nil {
		err = fmt.Errorf(
			"Error checking out reference '%s' from repository '%s': %s",
			finalName, url, err.Error(),
		)
		return err
	}

	return nil
}

func getNameAndCommit(
	ref *plumbing.Reference, repo *git.Repository,
) (
	name string, commit plumbing.Hash, err error,
) {

	name = ref.Name().Short()
	obj, err := repo.TagObject(ref.Hash()) // Annotated tags!
	switch err {
	case nil:
		// Its an annotated tag -> get its real commit
		commit = obj.Target
	case plumbing.ErrObjectNotFound:
		err = nil
		commit = ref.Hash()
	}
	return
}
