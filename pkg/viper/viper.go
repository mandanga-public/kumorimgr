//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package viper

import (
	viperLib "github.com/spf13/viper"
)

// Why this module?
// Viper uses "." as key delimiter, so it's a problem work with config files
// keys are like "kumori.systems/kumori"... and we want this kind of keys.
// But: Viper allows you to change the delimiter... but not in the global
// instance of Viper. Therefore, it is necessary to create a new instance of
// Viper, which we expose globally through this package.

var Global *viperLib.Viper

func init() {
	Global = viperLib.NewWithOptions(viperLib.KeyDelimiter("::"))
}
