//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

// Package cue2json transform a cue source into a json destination.
//
// IMPORTANT: FOR NOW, cue tool must be installed to use this package. In the
// future it will not be necessary.
//
// IMPORTANT: FOR NOW, cue module dependencies are not resolved here; this package
// assumes that dependencies are already resolved.
//
// We can use cuejson package to validate ("vet") the source, and to generate
// ("export") the json files.
// The destination directory will be created, if not exists.
//
// For example:
//
//  err = cue2json.Vet("./src")
//  if err != nil {
//    ...
//  }
//
//  err = cue2json.Export("./src", "./dst")
//  if err != nil {
//    ...
//  }
//
package cue2json
