//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package helper

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"syscall"

	"golang.org/x/crypto/ssh/terminal"
)

// FileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirExists checks if a dir exists and is directory before we
// try using it to prevent further errors.
func DirExists(dir string) bool {
	info, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// ChangeWorkDir changes the workdir, and returns the current workdir
func ChangeWorkDir(newWorkDir string) (oldWorkDir string, err error) {
	oldWorkDir, err = os.Getwd()
	if err != nil {
		return
	}
	err = os.Chdir(newWorkDir)
	return
}

// CreateFile creates a file (ensuring that the directory is created, if it does
// not exist) with the provided content
func CreateFile(
	fileFullName string, filePerm os.FileMode, content []byte,
) (
	err error,
) {
	filePath := filepath.Dir(fileFullName)
	err = os.MkdirAll(filePath, os.ModeDir|0777)
	if err != nil {
		return
	}
	err = ioutil.WriteFile(fileFullName, content, filePerm)
	if err != nil {
		return
	}
	return
}

// CreateFiles work like CreateFile, but for several files
func CreateFiles(
	fileFullNames []string, filePerm os.FileMode, contents [][]byte,
) (
	err error,
) {
	for k, fileFullName := range fileFullNames {
		filePath := filepath.Dir(fileFullName)
		content := contents[k]
		err = os.MkdirAll(filePath, os.ModeDir|0777)
		if err != nil {
			return
		}
		err = ioutil.WriteFile(fileFullName, content, filePerm)
		if err != nil {
			return
		}
	}
	return
}

// RenameIfExists renames a directory if exists, ensuring that the destination
// exists.
func RenameIfExists(src string, dst string) (err error) {
	if DirExists(src) {
		err = os.MkdirAll(filepath.Dir(dst), os.ModeDir|0777)
		if err != nil {
			return
		}
		err = os.Rename(src, dst)
		if err != nil {
			return
		}
	}
	return
}

// EmptyDir removes all contents from a given folder but not the folder itself
func EmptyDir(dir string) (int, error) {

	// Checks if the dst directoty is or not empty
	file, err := os.Open(dir)
	if err != nil {
		fmt.Printf("\n\nError: %s %s\n\n", dir, err.Error())
		return 0, err
	}
	files, err := file.Readdir(0)
	if err != nil {
		fmt.Printf("\n\nError: %s %s\n\n", dir, err.Error())
		return 0, err
	}

	// If the folder is already empty, nothing to do.
	if len(files) == 0 {
		return 0, nil
	}

	// Remove all content in the folder
	removed := 0
	for _, fileInfo := range files {
		filePath := path.Join(dir, fileInfo.Name())
		if fileInfo.IsDir() {
			err := os.RemoveAll(filePath)
			if err != nil {
				fmt.Printf("\n\nError: %s %s\n\n", filePath, err.Error())
				return removed, err
			}
			removed++
			continue
		}
		err := os.Remove(filePath)
		if err != nil {
			fmt.Printf("\n\nError: %s %s\n\n", filePath, err.Error())
			return removed, err
		}
		removed++
	}

	return removed, nil
}

// Copy function copy src file to dst file, checking that is a regular
// file (ensuring that the directory is created, if it does not exist)
func Copy(src string, dst string) (nbytes int64, err error) {
	nbytes = 0
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return
	}
	if !sourceFileStat.Mode().IsRegular() {
		err = fmt.Errorf("%s is not a regular file", src)
		return
	}
	source, err := os.Open(src)
	if err != nil {
		return
	}
	defer source.Close()
	filePath := filepath.Dir(dst)
	err = os.MkdirAll(filePath, os.ModeDir|0777)
	if err != nil {
		return
	}
	destination, err := os.Create(dst)
	if err != nil {
		return
	}
	defer destination.Close()
	nbytes, err = io.Copy(destination, source)
	return
}

// CopyDirContent copy the content of "src" directory to "dst" directory
// If "dst" directory not exists, it will be created
//
// TODO: UGLY IMPLEMENTATION. TO BE IMPROVED
//
func CopyDirContent(src string, dst string) (err error) {
	err = os.MkdirAll(dst, os.ModeDir|0777)
	if err != nil {
		return
	}
	cmd := exec.Command("cp", "-r", src, dst)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		return
	}
	return
}

func GetPasswd(question string) (password string, err error) {
	fmt.Print(question)
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return
	}
	fmt.Println()
	password = strings.TrimSpace(string(bytePassword))
	return
}
