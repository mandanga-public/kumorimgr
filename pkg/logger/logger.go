//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package logger

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var singleton *zap.SugaredLogger
var singletonCfg zap.Config

func Init(level string, output []string, encoding string, colorize bool) (err error) {

	singletonCfg.Encoding = encoding
	singletonCfg.OutputPaths = output
	singletonCfg.ErrorOutputPaths = output
	var encodeLevel zapcore.LevelEncoder
	if colorize == true {
		encodeLevel = zapcore.CapitalColorLevelEncoder
	} else {
		encodeLevel = zapcore.CapitalLevelEncoder
	}
	singletonCfg.EncoderConfig = zapcore.EncoderConfig{
		MessageKey:  "message",
		LevelKey:    "level",
		EncodeLevel: encodeLevel,
	}
	singletonCfg.Level = zap.NewAtomicLevel()
	SetLevel(level)

	logger, err := singletonCfg.Build()
	if err != nil {
		return
	}
	defer logger.Sync()

	zap.ReplaceGlobals(logger)
	singleton = zap.S()

	return
}

func SetLevel(level string) {
	switch level {
	case "debug":
		singletonCfg.Level.SetLevel(zap.DebugLevel)
	case "info":
		singletonCfg.Level.SetLevel(zap.InfoLevel)
	case "warn":
		singletonCfg.Level.SetLevel(zap.WarnLevel)
	case "error":
		singletonCfg.Level.SetLevel(zap.ErrorLevel)
	case "fatal":
		singletonCfg.Level.SetLevel(zap.FatalLevel)
	default:
		singletonCfg.Level.SetLevel(zap.InfoLevel)
	}
}

func Info(message string, fields ...interface{}) {
	singleton.Infow(message, fields...)
}

func Debug(message string, fields ...interface{}) {
	singleton.Debugw(message, fields...)
}

func Warn(message string, fields ...interface{}) {
	singleton.Warnw(message, fields...)
}

func Error(message string, fields ...interface{}) {
	singleton.Errorw(message, fields...)
	fmt.Println(message)
}

func Fatal(message string, fields ...interface{}) {
	singleton.Fatalw(message, fields...)
	fmt.Println(message)
}
