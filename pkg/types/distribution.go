//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package types

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Version = []int

type Ref struct {
	Version Version `json:"version"`
	Domain  string  `json:"domain"`
	Kind    string  `json:"kind"`
	Name    string  `json:"name"`
}

type PackageConfiguration map[string]interface{}

type Package struct {
	Name         string    `json:"name"`
	Version      string    `json:"version"`
	Dependencies []Package `json:"dependencies"`
}

type PackageDeployment struct {
	Package          Package              `json:"package"`
	Configuration    PackageConfiguration `json:"configuration"`
	DeploymentMethod interface{}          `json:"deploymentMethod"`
}

type Integration map[string][]PackageDeployment

type OSVersion struct {
	Flavour string `json:"flavour"`
	Version string `json:"version"`
	Kernel  string `json:"kernel"`
	Systemd string `json:"systemd"`
}

type Distribution struct {
	Ref         Ref                 `json:"ref"`
	KubeVersion string              `json:"kubeVersion"`
	OSVersion   OSVersion           `json:"osVersion"`
	Packages    Integration         `json:"packages"`
	PackageList []PackageDeployment `json:"packageList"`
}

func NewDistributionFromFile(filePath string) (distribution *Distribution, err error) {
	distribution = &Distribution{}
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}
	err = json.Unmarshal(content, distribution)
	return
}

func NewDistributionFromContent(content []byte) (distribution *Distribution, err error) {
	err = json.Unmarshal(content, distribution)
	return
}

func (d *Distribution) GetPackageConfig(name string) (cfg PackageConfiguration, err error) {
	for _, v := range d.PackageList {
		if v.Package.Name == name {
			cfg = v.Configuration
			return
		}
	}
	err = fmt.Errorf("Package %s not found", name)
	return
}
