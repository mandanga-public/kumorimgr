//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package types

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

const (
	MasterRole = "master"
	WorkerRole = "worker"
)

type InlineParams struct {
	UseEtcdBackup  string
	EtcdBackupFile string
}

type UpdateParams struct {
	AddMastersIPs    []string
	AddWorkersIPs    []string
	RemoveMastersIPs []string
	RemoveWorkersIPs []string
}

type ClusterAndDistribution struct {
	Cluster      Cluster      `json:"c"`
	Distribution Distribution `json:"d"`
	InlineParams InlineParams
	UpdateParams UpdateParams
}

func NewClusterAndDistributionFromFile(
	filePath string,
) (
	clusterAndDistribution *ClusterAndDistribution, err error,
) {
	clusterAndDistribution = &ClusterAndDistribution{
		InlineParams: InlineParams{
			UseEtcdBackup:  "false",
			EtcdBackupFile: "",
		},
		UpdateParams: UpdateParams{
			AddMastersIPs:    []string{},
			AddWorkersIPs:    []string{},
			RemoveMastersIPs: []string{},
			RemoveWorkersIPs: []string{},
		},
	}
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}
	err = json.Unmarshal(content, clusterAndDistribution)
	if err != nil {
		return
	}

	// Machines must appear just once	!
	IPMap := map[string]string{}
	for _, machine := range clusterAndDistribution.Cluster.Machines {
		if _, ok := IPMap[machine.IP]; ok == true {
			err = fmt.Errorf("Duplicated IP: %s", machine.IP)
			return
		}
		IPMap[machine.IP] = machine.IP // We use a map just for convenience
	}

	return
}

// -----------------------------------------------------------------------------
//
// Functions used from scripts go template.
// Values are returned in the format expected by the scripts
//
// -----------------------------------------------------------------------------

func (c *ClusterAndDistribution) GetAPIServerExternalIP() string {
	if c.Cluster.DNS.Management.Enabled && c.Cluster.APIServer.ExternalIP != nil {
		return "\"" + *c.Cluster.APIServer.ExternalIP + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetAPIServerInternalBalancing() string {
	if c.Cluster.APIServer.InternalIP != nil && *c.Cluster.APIServer.InternalIP != "" {
		return "\"true\""
	}
	return "\"false\""
}

func (c *ClusterAndDistribution) GetAPIServerHandleFloatingIP() string {
	if c.Cluster.APIServer.InternalIP != nil && *c.Cluster.APIServer.InternalIP != "" {
		return "\"true\""
	}
	return "\"false\""
}

func (c *ClusterAndDistribution) GetAPIServerInternalIP() string {
	if c.Cluster.APIServer.InternalIP != nil {
		return "\"" + *c.Cluster.APIServer.InternalIP + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetIngressExternalIP() string {
	if c.Cluster.DNS.Management.Enabled && c.Cluster.Ingress.ExternalIP != nil {
		return "\"" + *c.Cluster.Ingress.ExternalIP + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetIngressInternalBalancing() string {
	if c.Cluster.Ingress.InternalIP != nil && *c.Cluster.Ingress.InternalIP != "" {
		return "\"true\""
	}
	return "\"false\""
}

func (c *ClusterAndDistribution) GetIngressHandleFloatingIP() string {
	if c.Cluster.Ingress.InternalIP != nil && *c.Cluster.Ingress.InternalIP != "" {
		return "\"true\""
	}
	return "\"false\""
}

func (c *ClusterAndDistribution) GetIngressInternalIP() string {
	if c.Cluster.Ingress.InternalIP != nil {
		return "\"" + *c.Cluster.Ingress.InternalIP + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetCustomScriptsDirectory() string {
	if c.Cluster.CustomScripts.Enabled && c.Cluster.CustomScripts.Directory != nil {
		return "\"" + *c.Cluster.CustomScripts.Directory + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetDNSProviderName() string {
	if c.Cluster.DNS.Management.Enabled {
		if c.Cluster.DNS.Management.Provider != nil {
			provider := *c.Cluster.DNS.Management.Provider
			return "\"" + provider.Name + "\""
		}
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetDNSRoute53ConfigDir() string {
	if c.Cluster.DNS.Management.Enabled {
		if c.Cluster.DNS.Management.Provider != nil {
			provider := *c.Cluster.DNS.Management.Provider
			return "\"" + provider.Config.ConfigDir + "\""
		}
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetControllersHubRegistry() string {
	return "\"" + c.Cluster.ControllersHub.Hub + "\""
}

func (c *ClusterAndDistribution) GetControllersHubUsername() string {
	if c.Cluster.ControllersHub.Username != nil {
		return "\"" + *c.Cluster.ControllersHub.Username + "\""
	}
	return "\"\""
}

func (c *ClusterAndDistribution) GetControllersHubPassword() string {
	if c.Cluster.ControllersHub.Password != nil {
		return "\"" + *c.Cluster.ControllersHub.Password + "\""
	}
	return "\"\""
}
