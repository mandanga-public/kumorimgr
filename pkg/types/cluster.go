//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package types

import (
	"encoding/json"
	"io/ioutil"
)

type Noderole = string

type Label struct {
	Noderole string `json:"noderole"`
}

type Machine struct {
	IP     string  `json:"ip"`
	Labels []Label `json:"labels"`
}

func (m Machine) IsRole(role string) bool {
	for _, label := range m.Labels {
		if label.Noderole == role {
			return true
		}
	}
	return false
}

type SSH struct {
	User           string `json:"user"`
	PrivateKeyPath string `json:"privateKeyPath"`
}

type DNSProviderConfig struct {
	ConfigDir string `json:"configDir"`
}

type DNSProvider struct {
	Name   string            `json:"name"`
	Config DNSProviderConfig `json:"config"`
}

type DNSManagement struct {
	Enabled  bool         `json:"enabled"`
	Provider *DNSProvider `json:"provider"`
}

type DNS struct {
	ReferenceDomain string        `json:"referenceDomain"`
	Management      DNSManagement `json:"management"`
}

type Ingress struct {
	Nodes      []Machine `json:"nodes"`
	InternalIP *string   `json:"internalIP"`
	ExternalIP *string   `json:"externalIP"`
}

type APIServer struct {
	InternalIP *string `json:"internalIP"`
	ExternalIP *string `json:"externalIP"`
}

type CustomScripts struct {
	Enabled   bool    `json:"enabled"`
	Directory *string `json:"directory"`
}

type ControllersHub struct {
	Hub      string  `json:"hub"`
	Username *string `json:"username"`
	Password *string `json:"password"`
}

type Cluster struct {
	Name            string         `json:"name"`
	Machines        []Machine      `json:"machines"`
	SSH             SSH            `json:"ssh"`
	DockerBridgeNet string         `json:"dockerBridgeNet"`
	PodNet          string         `json:"podNet"`
	ClusterIPNet    string         `json:"clusterIPNet"`
	InternalDomain  string         `json:"internalDomain"`
	DNS             DNS            `json:"dns"`
	Ingress         Ingress        `json:"ingress"`
	APIServer       APIServer      `json:"apiServer"`
	CustomScripts   CustomScripts  `json:"customScripts"`
	ControllersHub  ControllersHub `json:"controllersHub"`
	DNSResolvers    []string       `json:"dnsResolvers"`
}

func NewClusterFromFile(filePath string) (cluster *Cluster, err error) {
	cluster = &Cluster{}
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}
	err = json.Unmarshal(content, cluster)
	return
}

func NewClusterFromContent(content []byte) (cluster *Cluster, err error) {
	err = json.Unmarshal(content, cluster)
	return
}
