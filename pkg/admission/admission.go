//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package admission

import (
	"cluster-manager/pkg/logger"
	"cluster-manager/pkg/viper"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/Jeffail/gabs"
)

// TODO: admission access viper configuration directly... good idea?

const defaultAdmissionTimeOut = time.Second * 60

func Login(user string, password string) (err error) {
	meth := "admission.Login"
	authorization := "Basic " + base64.StdEncoding.EncodeToString([]byte(user+":"+password))
	reqParams, err := NewRequestParamsUsingQueryParams()
	if err != nil {
		return
	}
	logger.Info("Init", "meth", meth)
	resJSON, err := doRequest("GET", "auth/login", authorization, reqParams)
	if err != nil {
		return
	}
	logger.Info("Done", "response", resJSON.String(), "meth", meth)
	if resJSON.Exists("message") { // Admission returns "message" when error
		err = fmt.Errorf(resJSON.Path("message").Data().(string))
		return
	}
	accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate, err :=
		tokensFromJSON(resJSON)
	if err != nil {
		return
	}
	err = writeTokens(
		accessToken, accessTokenExpiryDate,
		refreshToken, refreshTokenExpiryDate,
	)
	return
}

func Logout() (err error) {
	meth := "admission.Logout"
	logger.Info("Init", "meth", meth)
	return writeTokens("", "", "", "")
}

func RefreshTokenIfNeeded() (refreshed bool, err error) {
	meth := "admission.RefreshTokenIfNeeded"
	logger.Info("Init", "meth", meth)
	refreshed = false
	accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate, err := readTokens()
	if err != nil {
		return
	}
	if accessToken != "" && accessTokenExpiryDate.Before(time.Now()) {
		if refreshToken == "" || refreshTokenExpiryDate.Before(time.Now()) {
			fmt.Println("------------------------------------------------------------------------------")
			fmt.Println("Both access and refresh tokens have expired. Please use kumorictl login again.")
			fmt.Println("------------------------------------------------------------------------------")
			return
		}
		authorization := "Bearer " + accessToken
		var reqParams RequestParams
		reqParams, err = NewRequestParamsUsingFormParams(
			"grant_type", "refresh_token",
			"refresh_token", refreshToken,
		)
		if err != nil {
			return
		}
		var resJSON *gabs.Container
		resJSON, err = doRequest("POST", "auth/tokens/refresh", authorization, reqParams)
		if err != nil {
			return
		}
		logger.Info("Done", "response", resJSON.String(), "meth", meth)
		var accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate string
		accessToken, accessTokenExpiryDate, refreshToken, refreshTokenExpiryDate, err =
			tokensFromJSON(resJSON)
		if err != nil {
			return
		}
		err = writeTokens(
			accessToken, accessTokenExpiryDate,
			refreshToken, refreshTokenExpiryDate,
		)
		refreshed = true
		if err != nil {
			return
		}
	}
	return
}

func DescribeCluster() (state *gabs.Container, err error) {
	authorization, err := getAuthorization()
	if err != nil {
		return
	}
	requestParams, err := NewRequestParamsUsingQueryParams()
	if err != nil {
		return
	}
	response, err := doRequest("GET", "admission/clusterinfo", authorization, requestParams)
	if err != nil {
		return
	}
	err = analyzeResponseError(response)
	if err != nil {
		return
	}
	found := response.Exists("data")
	if !found {
		err = fmt.Errorf("Unexpected result. Original: %s", response.String())
		return
	}
	state, err = response.JSONPointer("/data")
	if err != nil {
		return
	}
	return
}

// analyzeResponseError analyzes response errors when response is like:
// { message: "bla bla bla", success: false, [...] }
func analyzeResponseError(response *gabs.Container) (err error) {
	success, found := response.Path("success").Data().(bool)
	if !found {
		err = fmt.Errorf("Unexpected result. Original: %s", response.String())
		return
	}
	if !success {
		message, found := response.Path("message").Data().(string)
		if !found {
			err = fmt.Errorf("Unexpected result. Original: %s", response.String())
			return
		}
		err = fmt.Errorf(message)
		return
	}
	return // No error!
}

func doRequest(
	httpMethod string, path string, authorization string, requestParams RequestParams,
) (
	resJSON *gabs.Container, err error,
) {
	meth := "admission.doRequest"
	logger.Debug("Requesting", "method", httpMethod, "path", path, "meth", meth)
	admissionURL, err := getAdmissionUrl()
	if err != nil {
		return
	}
	body, contentType, err := requestParams.GetBody()
	if err != nil {
		return
	}
	req, err := http.NewRequest(httpMethod, admissionURL+"/"+path, body)
	if err != nil {
		return
	}
	if contentType != "" {
		req.Header.Set("Content-Type", contentType)
	}
	if authorization != "" {
		req.Header.Set("Authorization", authorization)
	}
	httpClient := &http.Client{Timeout: defaultAdmissionTimeOut}
	res, err := httpClient.Do(req)
	if err != nil {
		return
	}
	defer res.Body.Close()
	resRaw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	if res.StatusCode != 200 {
		err = fmt.Errorf(string(resRaw))
		return
	}
	resJSON, err = gabs.ParseJSON(resRaw)
	if err != nil {
		err = fmt.Errorf(string(resRaw))
		return
	}
	return
}

func readTokens() (
	accessToken string, accessTokenExpiryDate time.Time,
	refreshToken string, refreshTokenExpiryDate time.Time,
	err error,
) {
	accessToken = viper.Global.GetString("access-token")
	accessTokenExpiryDate = time.Time{}
	if accessToken != "" {
		accessTokenExpiryDateStr := viper.Global.GetString("access-token-expiry-date")
		err = accessTokenExpiryDate.UnmarshalText([]byte(accessTokenExpiryDateStr))
		if err != nil {
			return
		}
	}
	refreshToken = viper.Global.GetString("refresh-token")
	refreshTokenExpiryDate = time.Time{}
	if refreshToken != "" {
		refreshTokenExpiryDateStr := viper.Global.GetString("refresh-token-expiry-date")
		err = refreshTokenExpiryDate.UnmarshalText([]byte(refreshTokenExpiryDateStr))
		if err != nil {
			return
		}
	}
	return
}

func writeTokens(
	accessToken string, accessTokenExpiryDate string,
	refreshToken string, refreshTokenExpiryDate string,
) (
	err error,
) {
	viper.Global.Set("access-token", accessToken)
	viper.Global.Set("access-token-expiry-date", accessTokenExpiryDate)
	viper.Global.Set("refresh-token", refreshToken)
	viper.Global.Set("refresh-token-expiry-date", refreshTokenExpiryDate)
	err = viper.Global.WriteConfig()
	if err != nil {
		err = fmt.Errorf(
			"Workspace initialization is mandatory to use this command (error %s)",
			err.Error(),
		)
		return
	}
	return
}

func getAdmissionUrl() (admissionURL string, err error) {
	admissionProtocol := viper.Global.GetString("admission-protocol")
	admissionHost := viper.Global.GetString("admission")
	if admissionProtocol == "" || admissionHost == "" {
		err = fmt.Errorf("Admission URL is empty")
	}
	admissionURL = admissionProtocol + "://" + admissionHost
	return
}

func getAuthorization() (authorization string, err error) {
	accessToken, _, _, _, err := readTokens()
	if err != nil {
		return
	}
	authorization = "Bearer " + accessToken
	return
}

func secondsToDateTimeString(numSeconds int) (dateTime string, err error) {
	duration := time.Duration(numSeconds) * time.Second
	dateTimeBytes, err := time.Now().Add(duration).MarshalText()
	if err != nil {
		return
	}
	dateTime = string(dateTimeBytes)
	return
}

func tokensFromJSON(
	objJSON *gabs.Container,
) (
	accessToken string, accessTokenExpiryDate string,
	refreshToken string, refreshTokenExpiryDate string,
	err error,
) {
	accessToken = objJSON.Path("access_token").Data().(string)
	accessTokenExpiresSeconds := int(objJSON.Path("expires_in").Data().(float64))
	accessTokenExpiryDate, err = secondsToDateTimeString(accessTokenExpiresSeconds)
	if err != nil {
		return
	}
	refreshToken = objJSON.Path("refresh_token").Data().(string)
	refreshTokenExpiresSeconds := int(objJSON.Path("refresh_expires_in").Data().(float64))
	refreshTokenExpiryDate, err = secondsToDateTimeString(refreshTokenExpiresSeconds)
	if err != nil {
		return
	}
	return
}
