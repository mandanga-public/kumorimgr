//Copyright (C) 2020 Kumori Systems S.L. All rights reserved.

package admission

import (
	"fmt"
	"io"
	"mime/multipart"
	"net/url"
	"os"
	"strings"
)

type FormData struct {
	Name     string
	FileName string
	FilePath string
}

type RequestParams struct {
	UseFormData    bool
	FormData       FormData
	UseFormParams  bool
	FormParams     map[string]string
	UseQueryParams bool
	QueryParams    map[string]string
}

func NewRequestParamsUsingFormData(
	name string, fileName string, filePath string,
) (
	requestParams RequestParams, err error,
) {
	requestParams = RequestParams{
		UseFormData: true,
		FormData: FormData{
			Name:     name,
			FileName: fileName,
			FilePath: filePath,
		},
		UseFormParams:  false,
		FormParams:     nil,
		UseQueryParams: false,
		QueryParams:    nil,
	}
	return
}

func NewRequestParamsUsingFormParams(
	params ...string,
) (
	requestParams RequestParams, err error,
) {
	formParams, err := arrayToStringMap(params)
	if err != nil {
		return
	}
	requestParams = RequestParams{
		UseFormData:    false,
		FormData:       FormData{},
		UseFormParams:  true,
		FormParams:     formParams,
		UseQueryParams: false,
		QueryParams:    nil,
	}
	return
}

func NewRequestParamsUsingQueryParams(
	params ...string,
) (
	requestParams RequestParams, err error,
) {
	queryParams, err := arrayToStringMap(params)
	if err != nil {
		return
	}
	requestParams = RequestParams{
		UseFormData:    false,
		FormData:       FormData{},
		UseFormParams:  false,
		FormParams:     nil,
		UseQueryParams: true,
		QueryParams:    queryParams,
	}
	return
}

func (rp RequestParams) GetBody() (body io.Reader, contentType string, err error) {
	if rp.UseFormData {
		reader, writer := io.Pipe()
		multipartWriter := multipart.NewWriter(writer)
		go func() { // Why goroutine? To avoid ram consumption in case of big files
			defer writer.Close()
			defer multipartWriter.Close()
			part, err := multipartWriter.CreateFormFile(rp.FormData.Name, rp.FormData.FileName)
			if err != nil {
				return
			}
			file, err := os.Open(rp.FormData.FilePath)
			if err != nil {
				return
			}
			defer file.Close()
			if _, err = io.Copy(part, file); err != nil {
				return
			}
		}()
		body = reader
		contentType = multipartWriter.FormDataContentType()
		return
	} else if rp.UseFormParams {
		formValue := url.Values{}
		for k, v := range rp.FormParams {
			formValue.Set(k, v)
		}
		body = strings.NewReader(formValue.Encode())
		contentType = "application/x-www-form-urlencoded"
	} else if rp.UseQueryParams {
		body = nil
		contentType = ""
	}
	return
}

func arrayToStringMap(paramsArray []string) (paramsMap map[string]string, err error) {
	if len(paramsArray)%2 != 0 {
		err = fmt.Errorf("Invalid number of parameteres")
		return
	}
	paramsMap = map[string]string{}
	for i := 0; i < len(paramsArray); i = i + 2 {
		paramsMap[paramsArray[i]] = paramsArray[i+1]
	}
	return
}
